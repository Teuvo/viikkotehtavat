package com.example.teppo.t75;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {

    Context context = null;
    EditText editText;
    EditText editTextFilename;
    TextView textView;

    Button button2, button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;

        editText = (EditText) findViewById(R.id.editText);
        editTextFilename = (EditText) findViewById(R.id.editTextFilename);

        textView = (TextView) findViewById(R.id.textView);
        button = (Button) findViewById(R.id.button);
        button2 = (Button) findViewById(R.id.button2);

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeFile(v, editText.getText().toString(), editTextFilename.getText().toString());
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                readFile(v, editTextFilename.getText().toString());
            }
        });

        /*editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String editTextText = editText.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editTextFilename.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String editTextFile = editTextFilename.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        //editTextFilename.addTextChangedListener()
        */
    }

    public void readFile(View v, String filename){
        try {
            //InputStream ins = context.openFileInput("testi.txt");

            InputStream ins = context.openFileInput(filename);

            BufferedReader br = new BufferedReader(new InputStreamReader(ins));
            String s = "";

            while ((s=br.readLine()) != null) {
                System.out.println(s);
                textView.setText(s);
            }
            ins.close();
        } catch (IOException e) {
            Log.e("IOException", "Virhe syötteessä");
        } finally {
            System.out.println("Luettu");
        }

    }

    public void writeFile(View v, String text, String filename) {


        try {
            OutputStreamWriter ows = new OutputStreamWriter(context.openFileOutput(filename, Context.MODE_PRIVATE));

            //String s = "";

            //s = "Tämä tulee tiedostoon \n Lue tiedosto jotta näet tämän \n tai sitten et näe mitään";
            ows.write(text);
            ows.close();
        } catch (IOException e) {
            Log.e("IOException", "Virhe syötteessä");
        } finally {
            System.out.println("Kirjoitettu");
        }

    }

    /*private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            String editTextFile = editTextFilename.getText().toString();
            String editTextText = editText.getText().toString();

        }
    };*/

}
