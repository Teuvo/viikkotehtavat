package com.example.teppo.viikko9;

import java.util.ArrayList;

public class CinemaThing {

    private ArrayList<Cinema> cinemaList;

    MovieThing mt = MovieThing.getInstance();

    private static CinemaThing ct = new CinemaThing();

    private ArrayList<Movie> m0vieList; //kokeilua


    private CinemaThing() {
        cinemaList = new ArrayList();
        cinemaList.add(new Cinema("Ison Kristiinan elokuvateatteri", mt.returnList()));
        cinemaList.add(new Cinema("Imatran elokuvateatteri", mt.returnList()));
        cinemaList.add(new Cinema("Skinnarilan elokuvateatteri", mt.returnList()));
        cinemaList.add(new Cinema("Sammonlahden elokuvateatteri", mt.returnList()));


        m0vieList = new ArrayList();
        m0vieList.add(new Movie("Cliffhanger", "Sali 1", "24.09.2019", "00.00"));
        m0vieList.add(new Movie("Mission impossible", "Sali 2", "28.02.2019", "15:00"));
        m0vieList.add(new Movie("Terminaattori3", "Sali 1", "23.05.2019", "22:00"));
        m0vieList.add(new Movie("Terminaattori4", "Sali 3", "27.02.2019", "16:00"));
        m0vieList.add(new Movie("Terminaattori5", "Sali 2", "2.04.2019", "18:00"));
        m0vieList.add(new Movie("Terminaattori6", "Sali 1", "11.01.2019", "19:00"));
        cinemaList.add(new Cinema("Lauritsalan elokuvateatteri", m0vieList)); // kokeilua




    }

    public static CinemaThing getInstance() {
        return ct;
    }

    public ArrayList<Movie> giveInfo(int i) {
        Cinema cinema = cinemaList.get(i);

        for (Cinema a: cinemaList) {
            System.out.println(a.getMovies());
        }
        //System.out.println(cinema.getMovies());

        ArrayList<Movie> movList = cinema.getMovies();

        return(movList);
    }


    public ArrayList<Cinema> returnList() {
        return cinemaList;
    }

}
