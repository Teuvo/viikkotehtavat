package com.example.teppo.viikko9;

import java.util.ArrayList;

public class MovieThing {

    private ArrayList<Movie> movieList;

    private static MovieThing mt = new MovieThing();

    private MovieThing() {
        movieList = new ArrayList();
        movieList.add(new Movie("Terminaattori", "Sali 1", "24.09.2019", "00.00"));
        movieList.add(new Movie("Terminaattori2", "Sali 2", "28.02.2019", "15:00"));
        movieList.add(new Movie("Terminaattori3", "Sali 1", "23.05.2019", "22:00"));
        movieList.add(new Movie("Terminaattori4", "Sali 3", "27.02.2019", "16:00"));
        movieList.add(new Movie("Terminaattori5", "Sali 2", "2.04.2019", "18:00"));
        movieList.add(new Movie("Terminaattori6", "Sali 1", "11.01.2019", "19:00"));



    }

    public static MovieThing getInstance() {
        return mt;
    }


    public ArrayList<Movie> returnList() {
        return movieList;
    }


}
