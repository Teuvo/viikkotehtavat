package com.example.teppo.viikko9;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;


import java.util.ArrayList;

import static com.example.teppo.viikko9.CinemaThing.*;

public class MainActivity extends AppCompatActivity {

    //private ArrayList<Movie> movielist;
    MovieThing mt = MovieThing.getInstance();
    CinemaThing ct = CinemaThing.getInstance();
    private ArrayAdapter mAdapter;
    Spinner spinner;
    private ArrayList<Cinema> cinemaArrayList;
    private ArrayList<String> cinemaList;
    private ArrayList<Movie> movielist;
    TextView textView;
    StringBuilder stringBuilder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cinemaList = new ArrayList<String>();
        textView = (TextView) findViewById(R.id.textView);
        stringBuilder = new StringBuilder();

        spinner =  findViewById(R.id.spinner);
        cinemaArrayList = ct.returnList();

        for (Cinema a : cinemaArrayList) {
                cinemaList.add(new String(a.getName()));
                //System.out.println(a.toString());
                //cinemaList.add("KUKKUU");
        }







        mAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1,cinemaList);
        spinner.setAdapter(mAdapter);

        mAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1,cinemaList);
        spinner.setAdapter(mAdapter);


    }


    public void Choice(View v) {
        int indeksi = spinner.getSelectedItemPosition();
        System.out.println(indeksi);
        movielist = ct.giveInfo(indeksi);
        //private String word;

        for (Movie a: movielist) {
            stringBuilder.append(a.getName() + " " + a.getPlace() + " " + a.getDate() + " " + a.getClock() + "\n");
            System.out.println(a.getName() + " " + a.getPlace() + " " + a.getDate() + " " + a.getClock() + "\n");
        }

        //word = stringBuilder.toString();
        textView.setText(stringBuilder.toString());
        stringBuilder.setLength(0);


    }
}
