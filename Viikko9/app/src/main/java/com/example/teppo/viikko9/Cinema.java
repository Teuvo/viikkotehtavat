package com.example.teppo.viikko9;

import java.util.ArrayList;

public class Cinema {

    private String name;
    MovieThing movieThing = MovieThing.getInstance();
    private ArrayList<Movie> leffaLista;

    public Cinema(String nimi, ArrayList<Movie> leffalista) {
        name = nimi;
        leffaLista = movieThing.returnList();

    }

    public String getName() {
        return name;
    }

    public ArrayList<Movie> getMovies() { return leffaLista; }


    /*@Override
    public String toString() {
        return (name);
    }*/

}
