package com.example.teppo.viikko9;

public class Movie {

    private String name;
    private String place;
    private String date;
    private String clock;


    public Movie(String nimi, String paikka, String pvm, String klo) {
        name = nimi;
        place = paikka;
        date = pvm;
        clock = klo;

    }


    public String getName() {
        return name;
    }

    public String getPlace() {
        return place;
    }

    public String getDate() { return date; }

    public String getClock() {
        return clock;
    }

    @Override
    public String toString() {
        return (name + " " + place + " " + date + " " + clock);
    }
}
