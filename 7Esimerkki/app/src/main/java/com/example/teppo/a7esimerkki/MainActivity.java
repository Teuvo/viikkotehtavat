package com.example.teppo.a7esimerkki;

import android.content.Context;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class MainActivity extends AppCompatActivity {

    Context context = null;

//TODO android:networkSecurityConfig="@xml/network_security_config" Tämä puuttuu manifestista, ei toimi siellä

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        System.out.println("KANSION SIJAINTI: " + context.getFilesDir());
    }

    public void readXML (View v) {
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            String urlString = "https://www.finnkino.fi/xml/Schedule/?area=1012&dt=12.11.2018";
            Document doc = builder.parse(urlString);
            doc.getDocumentElement().normalize();
            System.out.println("Root element: " +doc.getDocumentElement().getNodeName());

            NodeList nList = doc.getDocumentElement().getElementsByTagName("Show");

            for (int i = 0; i < nList.getLength() ; i++) {
                Node node = nList.item(i);
                System.out.println("Element is this: " + node.getNodeName());

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;

                    //TODO if elif systeemit tänne
                    System.out.print("Nimi: ");
                    System.out.println(element.getElementsByTagName("Title").item(0).getTextContent());

                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } finally {
            System.out.println("#######DONE#########");
        }

    }

    public void readFile(View v){
        try {
            InputStream ins = context.openFileInput("testi.txt"); //TODO Tälle arvo!!!

            BufferedReader br = new BufferedReader(new InputStreamReader(ins));
            String s = "";

            while ((s=br.readLine()) != null) {
                System.out.println(s);
            }
            ins.close();
        } catch (IOException e) {
            Log.e("IOException", "Virhe syötteessä");
        } finally {
            System.out.println("Luettu");
        }

    }

    public void writeFile(View v) {


        try {
            OutputStreamWriter ows = new OutputStreamWriter(context.openFileOutput("testi.txt", Context.MODE_PRIVATE));

            String s = "";

            s = "Tämä tulee tiedostoon \n Lue tiedosto jotta näet tämän \n tai sitten et näe mitään";
            ows.write(s);
            ows.close();
        } catch (IOException e) {
            Log.e("IOException", "Virhe syötteessä");
        } finally {
            System.out.println("Kirjoitettu");
        }

    }

}

