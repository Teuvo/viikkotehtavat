package com.example.teppo.viikko10;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    WebView web;
    EditText editText;
    String url;
    int index;
    ArrayList<String> pageList;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        web = findViewById(R.id.webView);
        web.setWebViewClient(new WebViewClient());
        web.getSettings().setJavaScriptEnabled(true);
        editText = findViewById(R.id.editText);
        pageList = new ArrayList<String>();
        textView = (TextView) findViewById(R.id.textView2);
        index = -1;
        url = "";

    }

    public void search(View v) {
        textView.setText("");

        if (editText.getText().toString().equals("index.html")) {
            url = ("file:///android_asset/index.html");
        } else if (editText.getText().toString().matches("(.*)" + "http://" + "(.*)")== true) {
            url = (editText.getText().toString());
        } else {
            url = ("http://" + editText.getText().toString());
        }



        if (index != (pageList.size() -1)) {
            for (int a = index; (pageList.size() -1) > a;) {
                pageList.remove(pageList.size() - 1);
            }
        }

        pageList.add(url);
        web.loadUrl(url);
        index ++;
    }

    public void refresh(View v) {
        textView.setText("");
        if (index == -1) {
            textView.setText("There's nothing to refresh!");
        } else {
            if (url.length() != 0)
                web.loadUrl(url);
        }
    }

    public void shoutOut(View v) {
        textView.setText("");
        if (url.equals("file:///android_asset/index.html") == true) {
            web.evaluateJavascript("javascript:shoutOut()", null);
        } else {
            textView.setText("Nothing happens");
        }
    }

    public void initialize(View v) {
        textView.setText("");

        if (url.equals("file:///android_asset/index.html") == true) {
            web.evaluateJavascript("javascript:initialize()", null);
        } else {
            textView.setText("Nothing happens");
        }
    }

    public void goBack (View v) {
        textView.setText("");
        if (index == 0 || index == -1) {
            textView.setText("We can't go backwards!");
        } else {
            index--;
            url = pageList.get(index);
            web.loadUrl(url);
            editText.setText(url);
        }
    }

    public void goForward (View v) {
        textView.setText("");
        System.out.println(pageList.size());
        if (index == (pageList.size()-1)) {
            textView.setText("We can't go forward!");
        } else {
            index ++;
            url = pageList.get(index);
            web.loadUrl(url);
            editText.setText(url);

        }


    }

}
