package com.example.teppo.viikko10esim;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends AppCompatActivity {

    WebView web;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        web = findViewById(R.id.webView);
        web.setWebViewClient(new WebViewClient());
        web.getSettings().setJavaScriptEnabled(true);
        //web.loadUrl("http://www.lut.fi");
        web.loadUrl("file:///android_asset/index.html");


    }
    public void executeJavascript(View v){
        web.evaluateJavascript("javascript:hello()", null);
    }



}
