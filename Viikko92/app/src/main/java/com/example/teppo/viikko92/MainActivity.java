package com.example.teppo.viikko92;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class MainActivity extends AppCompatActivity {

    CinemaManager cinemaManager = CinemaManager.getInstance();
    private ArrayAdapter mAdapter;
    private ArrayAdapter mAdapter2;
    Spinner spinner;
    Date thisDate;
    StringBuilder stringBuilder;
    TextView fromTimeTextView, toTimeTextView;
    ListView listView;
    EditText dateEditText, editTextTitle;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        spinner = findViewById(R.id.spinner);
        fromTimeTextView = findViewById(R.id.fromTimeTextView);
        toTimeTextView = findViewById(R.id.toTimeTextView);
        dateEditText = findViewById(R.id.dateEditText);
        editTextTitle = findViewById(R.id.editTextTitle);
        ArrayList<Cinema> cinemaList;

        thisDate = new Date();
        stringBuilder = new StringBuilder();
        listView = findViewById(R.id.listView);

        cinemaManager.firstXML();

        cinemaList = cinemaManager.returnList();
        mAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1,cinemaList);
        spinner.setAdapter(mAdapter);







    }


    /*public void pushTheButton(View v) throws ParseException {


        int indeksi = spinner.getSelectedItemPosition();
        String id = cinemaManager.giveId(indeksi);

        SimpleDateFormat osaKlokkan = new SimpleDateFormat("YYYY-MM-dd");
        String maxKlokkan = osaKlokkan.format(thisDate);
        String minKlokkan = (maxKlokkan + " 00:00");
        maxKlokkan = (maxKlokkan + " 23:59");

        System.out.println(maxKlokkan + "  ROARRRRRRRRRRRRR  " + minKlokkan);

        SimpleDateFormat klokkan = new SimpleDateFormat("YYYY-MM-dd hh:mm");

        SimpleDateFormat daattorna = new SimpleDateFormat("YYYY-MM-dd");



        Date maxi = klokkan.parse(maxKlokkan);
        Date mini = klokkan.parse(minKlokkan);


        if (fromTimeTextView.getText().toString().length() != 0) {

            if (dateEditText.getText().toString().length() == 0) {
                System.out.println("RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR ");
                System.out.println(daattorna.format(thisDate) + " " + fromTimeTextView.getText().toString());
                mini = klokkan.parse(daattorna.format(thisDate) + " " + fromTimeTextView.getText().toString());
            } else {

                String[] apu = dateEditText.getText().toString().split("\\.");
                System.out.println(apu[2] + "-" + apu[1] + "-" + apu[0] + " " + fromTimeTextView.getText().toString());
                System.out.println(mini);
                mini = klokkan.parse(apu[2] + "-" + apu[1] + "-" + apu[0] + " " + fromTimeTextView.getText().toString());
                System.out.println("KEKKULI");
            }
            System.out.println("HOX HOX HOX HOX HOX HOX HOX mini tulee tässä: " + mini + " HOX HOX HOX HOX");


        }
        if (toTimeTextView.getText().toString().length() != 0) {


            if (dateEditText.getText().toString().length() == 0) {
                System.out.println("RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR ");
                System.out.println(daattorna.format(thisDate) + " " + toTimeTextView.getText().toString());
                maxi = klokkan.parse(daattorna.format(thisDate) + " " + toTimeTextView.getText().toString());
            } else {

                String[] apu = dateEditText.getText().toString().split("\\.");
                System.out.println(apu[2] + "-" + apu[1] + "-" + apu[0] + " " + toTimeTextView.getText().toString());
                System.out.println(maxi);
                maxi = klokkan.parse(apu[2] + "-" + apu[1] + "-" + apu[0] + " " + toTimeTextView.getText().toString());
                System.out.println("JEKKULI");
            }
            System.out.println("HOX HOX HOX HOX HOX HOX HOX maxi tulee tässä: " + maxi + " HOX HOX HOX HOX" + "Maxin arvo on :" + maxi.getTime());


        }

        ArrayList<String> leffalista = new ArrayList<String>();

        try {

        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        String urlString;
        SimpleDateFormat dateForm = new SimpleDateFormat("dd.MM.YYYY");


        if (dateEditText.getText().length() == 0) {
            urlString = "https://www.finnkino.fi/xml/Schedule/?area=" + id + "&dt=" + dateForm.format(thisDate);
        } else {
            urlString = "https://www.finnkino.fi/xml/Schedule/?area=" + id + "&dt=" + dateEditText.getText();
        }

        Document doc = builder.parse(urlString);
        doc.getDocumentElement().normalize();

        NodeList nList = doc.getDocumentElement().getElementsByTagName("Show");

        for (int i = 0; i < nList.getLength() ; i++) {
            Node node = nList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;


                String klo = element.getElementsByTagName("dttmShowStart").item(0).getTextContent().replace("T"," ");
                System.out.println(klo);
                Date realKlokkan = klokkan.parse(klo);


                System.out.println("Tässä tulee maxi: " + maxi.getTime() + "Ja tässä sitten minimi: " + mini.getTime() + "Ja tässä reali: " + realKlokkan.getTime());
                System.out.println("Tässä tulee maxi: " + maxKlokkan + "Ja tässä sitten minimi: " + minKlokkan + "Ja tässä reali: " + klo);
                if ((realKlokkan.getTime() < maxi.getTime()) &&
                        realKlokkan.getTime() > mini.getTime()) {

                    String[] timma = element.getElementsByTagName("dttmShowStart").item(0).getTextContent().split("T");
                    String[] pvm = timma[0].split("-");

                    leffalista.add("Nimi: " + element.getElementsByTagName("Title").item(0).getTextContent() +
                            "\nPvm: " + pvm[2] + "." + pvm[1] + pvm[0] + "\nKello: " + timma[1] +
                            "\nPaikka: " + element.getElementsByTagName("TheatreAndAuditorium").item(0).getTextContent() + "\n");

                }

            }




        }

    } catch (IOException e) {
        e.printStackTrace();
    } catch (SAXException e) {
        e.printStackTrace();
    } catch (ParserConfigurationException e) {
        e.printStackTrace();
    } finally {
        System.out.println("%%%%%%%DONE$$$$$$$$$$");
    }

        mAdapter2 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, leffalista);
        listView.setAdapter(mAdapter2);



    }*/


    public void pushTheButton(View v) throws ParseException {


        int indeksi = spinner.getSelectedItemPosition();
        String id = cinemaManager.giveId(indeksi);

        System.out.println("t\n TÄSSÄ TÄMÄ IIDEE: " + id);

        SimpleDateFormat osaKlokkan = new SimpleDateFormat("YYYY-MM-dd");
        String maxKlokkan = osaKlokkan.format(thisDate);
        String minKlokkan = (maxKlokkan + " 00:00");
        maxKlokkan = (maxKlokkan + " 23:59");

        System.out.println(maxKlokkan + "  ROARRRRRRRRRRRRR  " + minKlokkan);

        SimpleDateFormat klokkan = new SimpleDateFormat("YYYY-MM-dd hh:mm");

        SimpleDateFormat daattorna = new SimpleDateFormat("YYYY-MM-dd");



        Date maxi = klokkan.parse(maxKlokkan);
        Date mini = klokkan.parse(minKlokkan);

        //TARVITAAN
        String timeIn = "";
        timeIn  = fromTimeTextView.getText().toString();
        String timeOut = "";
        timeOut = toTimeTextView.getText().toString();
        String titletext = "";
        titletext = editTextTitle.getText().toString();

        if (timeIn.length() != 0) {

            if (dateEditText.getText().toString().length() == 0) {
                System.out.println("RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR ");
                System.out.println(daattorna.format(thisDate) + " " + timeIn);
                mini = klokkan.parse(daattorna.format(thisDate) + " " + timeIn);
            } else {

                String[] apu = dateEditText.getText().toString().split("\\.");
                System.out.println(apu[2] + "-" + apu[1] + "-" + apu[0] + " " + timeIn);
                System.out.println(mini);
                mini = klokkan.parse(apu[2] + "-" + apu[1] + "-" + apu[0] + " " + timeIn);
                System.out.println("KEKKULI");
            }
            System.out.println("HOX HOX HOX HOX HOX HOX HOX mini tulee tässä: " + mini + " HOX HOX HOX HOX");


        }
        if (timeOut.length() != 0) {


            if (dateEditText.getText().toString().length() == 0) {
                System.out.println("RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR ");
                System.out.println(daattorna.format(thisDate) + " " + timeOut);
                maxi = klokkan.parse(daattorna.format(thisDate) + " " + timeOut);
            } else {

                String[] apu = dateEditText.getText().toString().split("\\.");
                System.out.println(apu[2] + "-" + apu[1] + "-" + apu[0] + " " + timeOut);
                System.out.println(maxi);
                maxi = klokkan.parse(apu[2] + "-" + apu[1] + "-" + apu[0] + " " + timeOut);
                System.out.println("JEKKULI");
            }
            System.out.println("HOX HOX HOX HOX HOX HOX HOX maxi tulee tässä: " + maxi + " HOX HOX HOX HOX" + "Maxin arvo on :" + maxi.getTime());


        }


        int dtl = dateEditText.getText().length();
        String dateEDIT = dateEditText.getText().toString();

        //TÄSSÄ KOHTI KUTSUTAAN MANAGERIA
        ArrayList<String> leffalista = new ArrayList<String>();

        cinemaManager.pushingTheButton( dtl, id,
                dateEDIT, klokkan, minKlokkan, maxKlokkan, maxi, mini, titletext);

        leffalista = cinemaManager.returnLeffalista();

        mAdapter2 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, leffalista);
        listView.setAdapter(mAdapter2);



    }



}
