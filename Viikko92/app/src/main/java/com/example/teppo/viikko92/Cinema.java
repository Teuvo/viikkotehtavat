package com.example.teppo.viikko92;

public class Cinema {
    private String place;
    private String id;


    public Cinema(String paikka, String ID) {
        place = paikka;
        id = ID;

    }


    public String getPlace() {
        return place;
    }

    public String getId() {
        return id;
    }



    @Override
    public String toString() {
        return ("Sijainti: " + place );
    }

}
