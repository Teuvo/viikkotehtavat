package com.example.teppo.viikko92;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class CinemaManager {

    private ArrayList<Cinema> cinemaArrayList;
    private ArrayList<String> leffalista;
    private ArrayList<String> teatteriIdLista;

    /*Spinner spinner;
    TextView fromTimeTextView, toTimeTextView;
    ListView listView;
    EditText dateEditText;*/

    Date thisDate;

    private static CinemaManager cm = new CinemaManager();

    private CinemaManager() {
        cinemaArrayList = new ArrayList<Cinema>();
        leffalista = new ArrayList<String>();
        teatteriIdLista = new ArrayList<String>();


        thisDate = new Date();


    }


    public static CinemaManager getInstance() {
        return cm;
    }

    public void addCinema(String paikka, String ID) {
        cinemaArrayList.add(new Cinema(paikka, ID));
    }


    public String giveId(int indeksi) {
        return cinemaArrayList.get(indeksi).getId();
    }

    public ArrayList<Cinema> returnList() {
        return cinemaArrayList;
    }

    public void firstXML() {
        try {

            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            String urlString1 = "https://www.finnkino.fi/xml/TheatreAreas/";
            Document doc = builder.parse(urlString1);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getDocumentElement().getElementsByTagName("TheatreArea");

            for (int i = 0; i < nList.getLength() ; i++) {
                Node node = nList.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    addCinema(element.getElementsByTagName("Name").item(0).getTextContent(), element.getElementsByTagName("ID").item(0).getTextContent());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void pushingTheButton(int dateLength, String id, String dateEDIT,
                                              SimpleDateFormat klokkan, String minKlokkan,
                                              String maxKlokkan, Date maxi, Date mini, String titletext) {
        try {

            leffalista.clear();
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            String urlString;
            SimpleDateFormat dateForm = new SimpleDateFormat("dd.MM.YYYY");




            if (id.equals("1029")) {
                teatteriIdLista.add("1014");
                teatteriIdLista.add("1015");
                teatteriIdLista.add("1016");
                teatteriIdLista.add("1017");
                teatteriIdLista.add("1041");
                teatteriIdLista.add("1018");
                teatteriIdLista.add("1019");
                teatteriIdLista.add("1021");
                teatteriIdLista.add("1022");

            } else {
                teatteriIdLista.add(id);
            }

            for (String a: teatteriIdLista) {
                System.out.println(a);

            }

            for (String a: teatteriIdLista) {


                if (dateLength == 0) {
                    urlString = "https://www.finnkino.fi/xml/Schedule/?area=" + a + "&dt=" + dateForm.format(thisDate);
                } else {
                    urlString = "https://www.finnkino.fi/xml/Schedule/?area=" + a + "&dt=" + dateEDIT;
                }



            Document doc = builder.parse(urlString);
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getDocumentElement().getElementsByTagName("Show");

            for (int i = 0; i < nList.getLength() ; i++) {
                Node node = nList.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;

                    String klo;
                    Date realKlokkan;
                    String[] timma, pvm;



                    klo = element.getElementsByTagName("dttmShowStart").item(0).getTextContent().replace("T", " ");
                    //System.out.println(klo);
                    realKlokkan = klokkan.parse(klo);

                    //System.out.println("Tässä tulee maxi: " + maxi.getTime() + "Ja tässä sitten minimi: " + mini.getTime() + "Ja tässä reali: " + realKlokkan.getTime());
                    //System.out.println("Tässä tulee maxi: " + maxKlokkan + "Ja tässä sitten minimi: " + minKlokkan + "Ja tässä reali: " + klo);
                    if ((realKlokkan.getTime() <= maxi.getTime()) &&
                            realKlokkan.getTime() >= mini.getTime()) {

                        timma = element.getElementsByTagName("dttmShowStart").item(0).getTextContent().split("T");
                        pvm = timma[0].split("-");

                        String title = element.getElementsByTagName("Title").item(0).getTextContent();

                        if (titletext.length() == 0) {
                            leffalista.add("Nimi: " + title +
                                    "\nPvm: " + pvm[2] + "." + pvm[1] + pvm[0] + "\nKello: " + timma[1] +
                                    "\nPaikka: " + element.getElementsByTagName("TheatreAndAuditorium").item(0).getTextContent() + "\n");
                        } else {
                            //if (titletext.equals(title)) {
                            if (title.matches("(.*)" + titletext + "(.*)")== true) {
                                leffalista.add("Nimi: " + title +
                                        "\nPvm: " + pvm[2] + "." + pvm[1] + pvm[0] + "\nKello: " + timma[1] +
                                        "\nPaikka: " + element.getElementsByTagName("TheatreAndAuditorium").item(0).getTextContent() + "\n");
                            }
                        }
                    }




                }
            }





            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            System.out.println("%%%%%%%DONE$$$$$$$$$$");
            teatteriIdLista.clear();
        }
    }

    public ArrayList<String> returnLeffalista() {
        return leffalista;
    }
/*public void pushingTheButton(View v) {


        int indeksi = spinner.getSelectedItemPosition();
        String id = giveId(indeksi);

        SimpleDateFormat osaKlokkan = new SimpleDateFormat("YYYY-MM-dd");
        String maxKlokkan = osaKlokkan.format(thisDate);
        String minKlokkan = (maxKlokkan + " 00:00");
        maxKlokkan = (maxKlokkan + " 23:59");

        System.out.println(maxKlokkan + "  ROARRRRRRRRRRRRR  " + minKlokkan);

        SimpleDateFormat klokkan = new SimpleDateFormat("YYYY-MM-dd hh:mm");

        SimpleDateFormat daattorna = new SimpleDateFormat("YYYY-MM-dd");



        Date maxi = klokkan.parse(maxKlokkan);
        Date mini = klokkan.parse(minKlokkan);


        if (fromTimeTextView.getText().toString().length() != 0) {

            if (dateEditText.getText().toString().length() == 0) {
                System.out.println("RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR ");
                System.out.println(daattorna.format(thisDate) + " " + fromTimeTextView.getText().toString());
                mini = klokkan.parse(daattorna.format(thisDate) + " " + fromTimeTextView.getText().toString());
            } else {

                String[] apu = dateEditText.getText().toString().split("\\.");
                System.out.println(apu[2] + "-" + apu[1] + "-" + apu[0] + " " + fromTimeTextView.getText().toString());
                System.out.println(mini);
                mini = klokkan.parse(apu[2] + "-" + apu[1] + "-" + apu[0] + " " + fromTimeTextView.getText().toString());
                System.out.println("KEKKULI");
            }
            System.out.println("HOX HOX HOX HOX HOX HOX HOX mini tulee tässä: " + mini + " HOX HOX HOX HOX");


        }
        if (toTimeTextView.getText().toString().length() != 0) {


            if (dateEditText.getText().toString().length() == 0) {
                System.out.println("RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR RUORRR ");
                System.out.println(daattorna.format(thisDate) + " " + toTimeTextView.getText().toString());
                maxi = klokkan.parse(daattorna.format(thisDate) + " " + toTimeTextView.getText().toString());
            } else {

                String[] apu = dateEditText.getText().toString().split("\\.");
                System.out.println(apu[2] + "-" + apu[1] + "-" + apu[0] + " " + toTimeTextView.getText().toString());
                System.out.println(maxi);
                maxi = klokkan.parse(apu[2] + "-" + apu[1] + "-" + apu[0] + " " + toTimeTextView.getText().toString());
                System.out.println("JEKKULI");
            }
            System.out.println("HOX HOX HOX HOX HOX HOX HOX maxi tulee tässä: " + maxi + " HOX HOX HOX HOX" + "Maxin arvo on :" + maxi.getTime());


        }

        ArrayList<String> leffalista = new ArrayList<String>();

        try {

            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            String urlString;
            SimpleDateFormat dateForm = new SimpleDateFormat("dd.MM.YYYY");


            if (dateEditText.getText().length() == 0) {
                urlString = "https://www.finnkino.fi/xml/Schedule/?area=" + id + "&dt=" + dateForm.format(thisDate);
            } else {
                urlString = "https://www.finnkino.fi/xml/Schedule/?area=" + id + "&dt=" + dateEditText.getText();
            }

            Document doc = builder.parse(urlString);
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getDocumentElement().getElementsByTagName("Show");

            for (int i = 0; i < nList.getLength() ; i++) {
                Node node = nList.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;


                    String klo = element.getElementsByTagName("dttmShowStart").item(0).getTextContent().replace("T"," ");
                    System.out.println(klo);
                    Date realKlokkan = klokkan.parse(klo);


                    System.out.println("Tässä tulee maxi: " + maxi.getTime() + "Ja tässä sitten minimi: " + mini.getTime() + "Ja tässä reali: " + realKlokkan.getTime());
                    System.out.println("Tässä tulee maxi: " + maxKlokkan + "Ja tässä sitten minimi: " + minKlokkan + "Ja tässä reali: " + klo);
                    if ((realKlokkan.getTime() < maxi.getTime()) &&
                            realKlokkan.getTime() > mini.getTime()) {

                        String[] timma = element.getElementsByTagName("dttmShowStart").item(0).getTextContent().split("T");
                        String[] pvm = timma[0].split("-");

                        leffalista.add("Nimi: " + element.getElementsByTagName("Title").item(0).getTextContent() +
                                "\nPvm: " + pvm[2] + "." + pvm[1] + pvm[0] + "\nKello: " + timma[1] +
                                "\nPaikka: " + element.getElementsByTagName("TheatreAndAuditorium").item(0).getTextContent() + "\n");

                    }

                }




            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } finally {
            System.out.println("%%%%%%%DONE$$$$$$$$$$");
        }

        mAdapter2 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, leffalista);
        listView.setAdapter(mAdapter2);

    }*/

}