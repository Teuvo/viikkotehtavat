package com.example.teppo.viikko11;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private DrawerLayout drawer;
    public static TextView matti;
    public static String edittexti;
    TextFragment textFragment = new TextFragment();
    //public static EditText edittext;

    @Override
    protected void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);

        //matti = findViewById(R.id.matti);
        System.out.println("ROAR" + matti);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    textFragment).commit();
            navigationView.setCheckedItem(R.id.nav_text);
        }


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()) {
            case R.id.nav_fontsize:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new FontsizeFragment()).commit();
                break;
            case R.id.nav_fontwidth:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new FontwidthFragment()).commit();
                //matti = findViewById(R.id.matti);

                break;
            case R.id.nav_fontheight:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new FontheightFragment()).commit();

                break;
            case R.id.nav_rows:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new RowsFragment()).commit();
                break;

            case R.id.nav_edittext:
                System.out.println("MORO");
                EditText texti =  findViewById(R.id.editText);
                System.out.println(texti.getText().toString());
                edittexti = texti.getText().toString();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new EdittextFragment()).commit();
                break;

            case R.id.nav_text:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        textFragment).commit();
                break;



        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    public void fontsize10(View v) {
        Bundle bundle=new Bundle();
        bundle.putString("message", "20");
        // myöskin näin voi bundle.putString("fontsize", "20");
        //set Fragmentclass Arguments
        textFragment.setArguments(bundle);
    }

    public void fontsize25(View v) {
        Bundle bundle=new Bundle();
        bundle.putString("message", "35");
        // myöskin näin voi bundle.putString("fontsize", "20");
        //set Fragmentclass Arguments
        textFragment.setArguments(bundle);
    }

    public void fontsize50(View v) {
        Bundle bundle=new Bundle();
        bundle.putString("message", "50");
        // myöskin näin voi bundle.putString("fontsize", "20");
        //set Fragmentclass Arguments
        textFragment.setArguments(bundle);
    }

    public void red(View v) {
        Bundle bundle = new Bundle();
        int color = Color.RED;
        String colorr = Integer.toString(color);
        bundle.putString("massage", colorr);
        textFragment.setArguments(bundle);
    }

    public void black(View v) {
        Bundle bundle = new Bundle();
        int color = Color.BLACK;
        String colorr = Integer.toString(color);
        bundle.putString("massage", colorr);
        textFragment.setArguments(bundle);
    }

    public void blue(View v) {
        Bundle bundle = new Bundle();
        int color = Color.BLUE;
        String colorr = Integer.toString(color);
        bundle.putString("massage", colorr);
        textFragment.setArguments(bundle);
    }

    public void minlines(View v) {
        Bundle bundle = new Bundle();
        bundle.putString("amountoflines", "3");
        textFragment.setArguments(bundle);
    }
    public void medlines(View v) {
        Bundle bundle = new Bundle();
        bundle.putString("amountoflines", "5");
        textFragment.setArguments(bundle);
    }
    public void maxlines(View v) {
        Bundle bundle = new Bundle();
        bundle.putString("amountoflines", "10");
        textFragment.setArguments(bundle);
    }

    public void yesyes(View v) {
        Bundle bundle = new Bundle();
        bundle.putString("capscaps", "true");
        textFragment.setArguments(bundle);
    }

    public void nono(View v) {
        Bundle bundle = new Bundle();
        bundle.putString("capscaps", "false");
        textFragment.setArguments(bundle);
    }

    public void enableEditing(View v) {
        Bundle bundle = new Bundle();
        bundle.putString("enableEdittext", "0");
        textFragment.setArguments(bundle);


    }

    public void disableEditing(View v) {
        Bundle bundle = new Bundle();
        bundle.putString("edittext", edittexti);
        textFragment.setArguments(bundle);

        //edittexti = "";
        bundle.putString("edittextEmpty", "JAHUUU");
        textFragment.setArguments(bundle);
    }


}
