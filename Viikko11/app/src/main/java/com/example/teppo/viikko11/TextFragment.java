package com.example.teppo.viikko11;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.KeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class TextFragment extends Fragment {

    KeyListener listener;
    TextView matti;
    EditText editText;
    ArrayList messageArrayList = new ArrayList();
    int luku = 35;
    int color = Color.RED;
    int lines = 10;
    boolean caps = false;
    int index = 0;
    String matinteksti = "matti matti matti matti matti matti matti matti matti matti matti matti matti";
    //String teksti = "matti matti matti matti matti matti matti matti matti matti matti";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        String strtext = "";
        String strtext2 = "";
        String strtext3 = "";
        String strtext4 = "";

        try {
            strtext=getArguments().getString("message");
            luku = Integer.parseInt(strtext);
        } catch (Exception e) {}

        try {
            strtext2=getArguments().getString("massage");
            color = Integer.parseInt(strtext2);
        } catch (Exception e) {}

        try {
            strtext3=getArguments().getString("amountoflines");
            lines = Integer.parseInt(strtext3);
        } catch (Exception e) {}

        try {
            strtext4=getArguments().getString("capscaps");
            if (strtext4.equals("true")) {
                //teksti = "matti matti matti matti matti matti matti matti matti matti matti";
                caps = true;
            } else if (strtext4.equals("false")) {
                caps = false;
            }
        } catch (Exception e) {}




        System.out.println("VIESTI!" + strtext);
        //messageArrayList.add(strtext);

        return inflater.inflate(R.layout.fragment_text, container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        String strtext5 = "";
        String strtext6 = "";
        String strtext7 = "";
        matti = getView().findViewById(R.id.matti);
        editText = getView().findViewById(R.id.editText);
        listener = editText.getKeyListener();
        System.out.println("HOIJOIHOI " + listener + " HOIHOI");
        System.out.println(editText);
        System.out.println("EDITTEXT: " + editText.getText().toString());
        //System.out.println("MATTI");
        //MainActivity.matti.setText("SEPPO");
        //MainActivity.matti.setTextSize(50);
        //MainActivity.matti.setWidth(5000);
        //MainActivity.matti.setHeight(5000);
        //MainActivity.matti.setLines(7);
        //int luku = Integer.parseInt(messageArrayList.get(0).toString());
        matti.setTextSize(luku);
        matti.setTextColor(color);
        matti.setLines(lines);




        //editText.setKeyListener(null);



        try {
            strtext5=getArguments().getString("edittext");
            System.out.println(strtext5);
            System.out.println(matti.getText());
            System.out.println("HOX HOX HOX HXO");
            editText.getText().append("MOOI");
            System.out.println("EDITTEXT: " + editText.getText().toString());
            System.out.println("Matintekstiä ennen: " + matinteksti);
            if (strtext5 != null)
            matinteksti = strtext5;
            System.out.println("edittextiä jälkeen: " + editText.getText().toString());
            //editText.setText("");
        } catch (Exception e) {}

        try {
            strtext6 = getArguments().getString("edittextEmpty");
            editText.setText(strtext6);
            System.out.println("HOTHMROHMRHMOR " + strtext6 + " hHSRMIOTHORHMIRSHTMSR");
            //listener = editText.getKeyListener();
            editText.setKeyListener(null);
            //index = 1;

        } catch (Exception e) {}

        try {
            strtext7 = getArguments().getString("enableEdittext");
            index = Integer.parseInt(strtext7);
            if (index == 0)
            editText.setKeyListener(listener);
            System.out.println("HKEHEKHEK " + listener + " KHEHKEOHK");

        } catch (Exception e) {}

        //if (matinteksti.equals("")){
        //    matinteksti = editText.getText().toString();
        //}

        System.out.println("Tässä matinteksti:" + matinteksti);
        matti.setText(matinteksti);
        matti.setAllCaps(caps);
        System.out.println("Tässä vielä edittextiä" + editText.getText().toString());
        //editText.setText(strtext6);
        //editText.setText("JEPPIS");

        //matti.setText(teksti);



    }

    public void adjustSize(View v) {
        MainActivity.matti = getView().findViewById(R.id.matti);
        MainActivity.matti.setTextSize(5000);
    }




}
