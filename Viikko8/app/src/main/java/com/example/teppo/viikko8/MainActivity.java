package com.example.teppo.viikko8;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity {

    BottleDispenser bottledispenser = BottleDispenser.getInstance();
    //BottleDispenser bottledispenser = new BottleDispenser();
    Context context = null;
    private ArrayList<Bottle> botlList;
    private ArrayAdapter mAdapter;
    Spinner spinner;
    TextView textView, textView2;
    private SeekBar seekBar;
    private int progressvalue;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinner =  findViewById(R.id.spinner);
        textView = (TextView) findViewById(R.id.textView);
        textView2 = (TextView) findViewById(R.id.textView2);
        seekBar = (SeekBar)findViewById(R.id.seekBar);
        context = MainActivity.this;
        botlList = bottledispenser.returnList();

        /*botlList = new ArrayList<>();

        botlList.add(new Bottle("Pepsi Max", "Pepsi", 0.5, 1.8));
        botlList.add(new Bottle("Pepsi Max", "Pepsi", 1.5, 2.2));
        botlList.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.5, 2.0));
        botlList.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 1.5, 2.5));
        botlList.add(new Bottle("Fanta Zero", "Fanta", 0.5, 1.95));
        botlList.add(new Bottle("Fanta Zero", "Fanta", 0.5, 1.95));*/


        //recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        //recyclerView.setLayoutManager(layoutManager);

        mAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1,botlList);
        spinner.setAdapter(mAdapter);

        //textView2.setText(seekBar.getProgress() + " / " + seekBar.getMax());

        seekBar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {

                    //int progressvalue;
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        progressvalue = progress;
                        textView2.setText(progress*0.5 + "€ / " + seekBar.getMax()*0.5 + "€");


                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        //textView2.setText(progressvalue*0.5 + " / " + seekBar.getMax()*0.5);

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        //textView2.setText(progressvalue*0.5 + " / " + seekBar.getMax()*0.5);


                    }
                });
        seekBar.setProgress(0);


                //BottleDispenser bottledispenser = new BottleDispenser();


    }

    public void lisaa(View v) {
        //BottleDispenser bottledispenser = new BottleDispenser();
        double progressio = progressvalue;
        progressio = progressio*0.5;
        bottledispenser.addMoney(progressio);
        seekBar.setProgress(0);
        textView.setText("Klink! " + progressio + "€ Lisää rahaa laitteeseen!");


    }

    public void osta(View v) {

        int indeksi = spinner.getSelectedItemPosition();
        if (botlList.isEmpty()) {
            textView.setText("Pullot loppu!");

        } else {
            //textView.setText(bottledispenser.buyBottle(indeksi));
            String sana = bottledispenser.buyBottle(indeksi);

            if (sana.equals("OutOfMoney")) {
                textView.setText("Lisää rahaa ensin!");
            } else {

                String[] osat = sana.split(",");
                String nimi = osat[0];
                String koko = osat[1];
                String hinta = osat[2];
                String valmistaja = osat[3];
                textView.setText("Kachunk! " + nimi + " " + koko + "l " + hinta + "€ tipahti masiinasta!");
                try {
                    OutputStreamWriter ows = new OutputStreamWriter(context.openFileOutput("kuitti.txt", Context.MODE_PRIVATE));
                    ows.write("KUITTI\nTuote: " + nimi + "\nHinta: " + hinta + "€\nKoko: " + koko
                            + "l\nValmistaja: " + valmistaja);
                    ows.close();
                } catch (IOException e) {
                    Log.e("IOException", "Virhe syötteessä");
                } finally {
                    //textView.setText(bottledispenser.buyBottle(indeksi) + "\nKuitti tulostettu tiedostoon kuitti.txt");
                }

            }
        }

        //System.out.println("tässä tämä " + indeksi);

        //bottledispenser.buyBottle(indeksi);


    }

    public void palauta(View v) {
        //BottleDispenser bottledispenser = new BottleDispenser();

        double massit = bottledispenser.returnMoney();
        DecimalFormat df = new DecimalFormat("#.##");

        textView.setText("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + df.format(massit) + "€");
    }

    public void sulkija(View v) {
        System.exit(1);
    }

}
