package com.example.teppo.viikko8;

import java.util.ArrayList;

public class BottleDispenser {

    private int bottles;
    private double money;
    private ArrayList<Bottle> bottle_list;


    private static BottleDispenser bd = new BottleDispenser();

    private BottleDispenser() {
        bottles = 6;
        money = 0;
        bottle_list = new ArrayList();
        bottle_list.add(new Bottle("Pepsi Max", "Pepsi", 0.5, 1.8));
        bottle_list.add(new Bottle("Pepsi Max", "Pepsi", 1.5, 2.2));
        bottle_list.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.5, 2.0));
        bottle_list.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 1.5, 2.5));
        bottle_list.add(new Bottle("Fanta Zero", "Fanta", 0.5, 1.95));
        bottle_list.add(new Bottle("Fanta Zero", "Fanta", 0.5, 1.95));

    }

    public static BottleDispenser getInstance() {
        return bd;
    }


    /*private int bottles;
    private double money;

    private ArrayList<Bottle> bottle_list;

    public BottleDispenser() {
        bottles = 6;
        money = 0;
        bottle_list = new ArrayList();
        bottle_list.add(new Bottle("Pepsi Max", "Pepsi", 0.5, 1.8));
        bottle_list.add(new Bottle("Pepsi Max", "Pepsi", 1.5, 2.2));
        bottle_list.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.5, 2.0));
        bottle_list.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 1.5, 2.5));
        bottle_list.add(new Bottle("Fanta Zero", "Fanta", 0.5, 1.95));
        bottle_list.add(new Bottle("Fanta Zero", "Fanta", 0.5, 1.95));
    }*/

    public void addMoney(double i){
        money = money + i;
        //System.out.println("Klink! Lisää rahaa laitteeseen!");
    }


    public String buyBottle(int i) {
        //Bottle bottle = bottle_list.get(i-1);
        Bottle bottle = bottle_list.get(i);

        //System.out.println("Tässä vielä bottledispenseristä tämä " + (i));
        if (money < bottle.getPrice()) {
            //System.out.println("Syötä rahaa ensin!");
            return ("OutOfMoney");
        } else {
            bottles -= 1;
            money -= bottle.getPrice();

            //System.out.println("KACHUNK!" + bottle_list.getName() + "tipahti masiinasta!");
            //System.out.println("KACHUNK! vähän pepsiä tipahti masiinasta!");
            ////System.out.print("KACHUNK! ");
            ////System.out.print(bottle.getName());
            ////System.out.println(" tipahti masiinasta!");
            bottle_list.remove(i);
            return(bottle.getName() + "," + bottle.getSize() + "," + bottle.getPrice() + "," + bottle.getManufacturer());


        }
    }

    public double returnMoney() {
        //System.out.printf("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %1.2f€\n", money);
        double mon = money;
        money = 0;
        return mon;
    }

    public void printBottles() {
        for (int i=0; i<bottles; i++) {
            Bottle b = bottle_list.get(i);
            System.out.print(i+1 + ". Nimi: " + b.getName() + "\n\tKoko: " +
                    b.getSize() + "\tHinta: " + b.getPrice() + "\n");

        }
    }

    public ArrayList<Bottle> returnList() {
        return bottle_list;
    }

}
