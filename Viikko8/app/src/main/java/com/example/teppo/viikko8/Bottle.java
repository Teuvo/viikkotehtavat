package com.example.teppo.viikko8;

public class Bottle {

    private String name;
    private String manufacturer;
    private double size;
    private double price;

    /*public Bottle() {
        name = "Pepsi Max";
        manufacturer = "Pepsi";
        size = 0.5;
        price = 1.80;
    }*/





    public Bottle(String nimi, String valmistaja, double koko, double hinta) {
        name = nimi;
        manufacturer = valmistaja;
        size = koko;
        price = hinta;

    }


    public String getName() {
        return name;
    }

    public String getManufacturer() {
        return manufacturer;
    }


    public double getSize() {
        return size;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return (name + " " + manufacturer + " " + size + "l " + price + "€" );
    }


}
