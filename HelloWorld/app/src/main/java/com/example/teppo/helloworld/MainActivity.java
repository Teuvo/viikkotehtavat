package com.example.teppo.helloworld;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

    TextView text;
    TextView text2;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //this.test();
        text = (TextView) findViewById(R.id.textView);
        text2 = (TextView) findViewById(R.id.textView2);
        editText = (EditText) findViewById(R.id.editText);

        editText.addTextChangedListener(textWatcher);

        //text.setText("Heipähei!");
    }

    public void test(View v) {
        System.out.println("Hello World!");
        text.setText("Hello World!");
    }

    public void testAdder(View v) {
        EditText editText = (EditText) findViewById(R.id.editText);

        text.setText(editText.getText().toString());


    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String input = editText.getText().toString();

            text2.setText(input);

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };







}
